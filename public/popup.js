$(document).ready(function() { 
	
	$(".popup_bg").click(function(){
        
		$(".popup").fadeOut(800);	
	});
});

$('.close').click(function() {
    $(".popup").fadeOut(800);
     history.pushState(false,' ','.');
})

$(function(){
    $(".ajaxForm").submit(function(e){
        e.preventDefault();
        var href = $(this).attr("action");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("We received your submission, thank you!");
					document.querySelector('form').reset()
                }else{
                    alert("An error occured.");
                }
            }
			
        });
    });
});

function showPopup() {
	$(".popup").fadeIn(800); 
            history.pushState({ page: 1 }, "tttt1", "?modal");
            try {
                inputs[0].value = localStorage.key(localStorage.length - 1);
                inputs[1].value = localStorage.getItem(localStorage.key(localStorage.length - 1));
            } finally {
                statusMessage.innerHTML = '';

                e.preventDefault();

                var modalId = this.getAttribute('data-modal'),
                    modalElem = document.querySelector('.modal[data-modal="' + modalId + '"]');

                modalElem.classList.add('active');
                overlay.classList.add('active');
            }


}
        History.Adapter.bind(window, 'statechange', function(){
            var State = History.getState();
            loadPage(State.url);
        });

